import numpy as np


class GeneticAlgorithm(object):
    """
		Implement a simple generationl genetic algorithm as described in the instructions
	"""

    def __init__(self, chromosomeShape,
                 errorFunction,
                 elitism=1,
                 populationSize=25,
                 mutationProbability=.1,
                 mutationScale=.5,
                 numIterations=10000,
                 errorTreshold=1e-6
                 ):

        self.populationSize = populationSize  # size of the population of units
        self.p = mutationProbability  # probability of mutation
        self.numIter = numIterations  # maximum number of iterations
        self.e = errorTreshold  # threshold of error while iterating
        self.f = errorFunction  # the error function (reversely proportionl to fitness)
        self.keep = elitism  # number of units to keep for elitism
        self.k = mutationScale  # scale of the gaussian noise

        self.i = 0  # iteration counter

        # initialize the population randomly from a gaussian distribution
        # with noise 0.1 and then sort the values and store them internally

        self.population = []
        for _ in range(populationSize):
            chromosome = np.random.randn(chromosomeShape) * 0.1

            fitness = self.calculateFitness(chromosome)
            self.population.append((chromosome, fitness))

        # sort descending according to fitness (larger is better)
        self.population = sorted(self.population, key=lambda t: -t[1])

    def step(self):
        """
			Run one iteration of the genetic algorithm. In a single iteration,
			you should create a whole new population by first keeping the best
			units as defined by elitism, then iteratively select parents from
			the current population, apply crossover and then mutation.

			The step function should return, as a tuple: 
				
			* boolean value indicating should the iteration stop (True if 
				the learning process is finished, False othwerise)
			* an integer representing the current iteration of the 
				algorithm
			* the weights of the best unit in the current iteration

		"""

        self.i += 1

        newPopulation = self.bestN(min(self.populationSize, self.keep))

        while (len(newPopulation) < self.populationSize):
            parents = self.selectParents()

            newChromosome = self.crossover(parents[0], parents[1])

            newChromosome = self.mutate(newChromosome)

            fitness = self.calculateFitness(np.array(newChromosome))

            newPopulation.append((np.array(newChromosome), fitness))

        self.population = sorted(newPopulation, key=lambda t: -t[1])

        return (self.i == self.numIter, self.i + 1, self.best()[0])

    def fitnessSum(self):
        sum = float(0)
        for chromosome in self.population:
            sum += chromosome[1]
        return sum

    def calculateFitness(self, chromosome):
        """
			Implement a fitness metric as a function of the error of
			a unit. Remember - fitness is larger as the unit is better!
		"""

        chromosomeError = self.f(chromosome)
        fitness = float(1) / chromosomeError
        return fitness

    def bestN(self, n):
        """
			Return the best n units from the population
		"""
        return self.population[0:n]

    def best(self):
        return self.population[0]

    def selectParents(self):
        """
			Select two parents from the population with probability of 
			selection proportional to the fitness of the units in the
			population		
		"""
        # select first parent
        probabilites = []

        fitnessSum = self.fitnessSum()

        for chromosome in self.population:
            probabilites.append(chromosome[1][0] / fitnessSum[0])

        indexes = range(0, self.populationSize)
        parentIndexes = np.random.choice(indexes, 2, False, probabilites)
        parents = map(lambda x: self.population[x][0], parentIndexes)
        return parents

    def crossover(self, p1, p2):
        """
			Given two parent units p1 and p2, do a simple crossover by 
			averaging their values in order to create a new child unit
		"""
        newChromosome = (p1 + p2) / 2

        return newChromosome

    def mutate(self, chromosome):
        """
			Given a unit, mutate its values by applying gaussian noise
			according to the parameter k
		"""

        mutatedChromosome = []
        for weight in chromosome:
            if np.random.choice((True, False), p=[self.p, 1 - self.p]):
                weight += np.random.normal(0, self.k)
            mutatedChromosome.append(weight)
        return mutatedChromosome
