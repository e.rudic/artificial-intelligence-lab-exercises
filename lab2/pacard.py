"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
from logic import *


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
        actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def miniWumpusSearch(problem):
    """
    A sample pass through the miniWumpus layout. Your solution will not contain 
    just three steps! Optimality is not the concern here.
    """
    from game import Directions
    e = Directions.EAST
    n = Directions.NORTH
    return [e, n, n]


def logicBasedSearch(problem):
    """

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    print "Does the Wumpus's stench reach my spot?", 
               \ problem.isWumpusClose(problem.getStartState())

    print "Can I sense the chemicals from the pills?", 
               \ problem.isPoisonCapsuleClose(problem.getStartState())

    print "Can I see the glow from the teleporter?", 
               \ problem.isTeleporterClose(problem.getStartState())
    
    (the slash '\\' is used to combine commands spanning through multiple lines - 
    you should remove it if you convert the commands to a single line)
    
    Feel free to create and use as many helper functions as you want.

    A couple of hints: 
        * Use the getSuccessors method, not only when you are looking for states 
        you can transition into. In case you want to resolve if a poisoned pill is 
        at a certain state, it might be easy to check if you can sense the chemicals 
        on all cells surrounding the state. 
        * Memorize information, often and thoroughly. Dictionaries are your friends and 
        states (tuples) can be used as keys.
        * Keep track of the states you visit in order. You do NOT need to remember the
        tranisitions - simply pass the visited states to the 'reconstructPath' method 
        in the search problem. Check logicAgents.py and search.py for implementation.
    """
    # array in order to keep the ordering
    visitedStates = []
    startState = problem.getStartState()
    visitedStates.append(startState)

    currentState = startState;
    if (problem.isGoalState(startState)):
        print "oh yeah"

    stateDict = dict()
    ruleClausesDict = dict()

    stateDict.update({currentState: scgClause(problem, currentState)})

    while currentState and (Literal('t', currentState, False) not in stateDict.get(currentState)):
        successors = set()
        for successor in problem.getSuccessors(currentState):
            successors.add(successor[0])

            updateDict(scgClause(problem, successor[0]), successor[0], stateDict)
            updateDict(ruleClauses(problem, successor[0]), successor[0], ruleClausesDict)

        for successor in problem.getSuccessors(currentState):
            for predictionState in problem.getSuccessors(successor[0]):
                clauses = set()

                updateDict(ruleClauses(problem, predictionState[0]), predictionState[0], ruleClausesDict)
                clauses = clauses.union(ruleClausesDict.get(predictionState[0]))

                predictionStateClauses = stateDict.get(predictionState[0])
                if predictionStateClauses:
                    clauses = clauses.union(predictionStateClauses)

                for neighbour in problem.getSuccessors(predictionState[0]):
                    neighbourClauses = stateDict.get(neighbour[0])
                    updateDict(ruleClauses(problem,neighbour[0]),neighbour[0],ruleClausesDict)
                    if neighbourClauses:
                        clauses = clauses.union(neighbourClauses)
                    clauses.union(ruleClausesDict.get(neighbour[0]))


                wumpus = Clause(Literal('w', predictionState[0], False))
                notWumpus = Clause(Literal('w', predictionState[0], True))
                pill = Clause(Literal('p', predictionState[0], False))
                notPill = Clause(Literal('p', predictionState[0], True))
                teleporter = Clause(Literal('t', predictionState[0], False))
                notTeleporter = Clause(Literal('t', predictionState[0], True))
                safe = Clause(Literal('o', predictionState[0], False))
                notSafe = Clause(Literal('o', predictionState[0], True))

                if (wumpus or notWumpus) not in clauses:
                    if resolution(clauses, wumpus):
                        update(clauses, wumpus, predictionState[0], stateDict)
                        print "wumpus found at", predictionState[0]

                    elif resolution(clauses, notWumpus):
                        update(clauses, notWumpus, predictionState[0], stateDict)
                        print "wumpus NOT at", predictionState[0]

                if (pill or notPill) not in clauses:

                    if resolution(clauses, pill):
                        update(clauses, pill, predictionState[0], stateDict)
                        print "pill found at", predictionState[0]

                    elif resolution(clauses, notPill):
                        update(clauses, notPill, predictionState[0], stateDict)
                        print "pill NOT at", predictionState[0]

                if (teleporter or notTeleporter) not in clauses:
                    if resolution(clauses, teleporter):
                        update(clauses, teleporter, predictionState[0], stateDict)
                        print "teleporter found at", predictionState[0]

                    elif resolution(clauses, notTeleporter):
                        update(clauses, notTeleporter, predictionState[0], stateDict)
                        print "teleporter NOT at", predictionState[0]

                if (safe or notSafe) not in clauses:
                    if resolution(clauses, safe):
                        update(clauses, safe, predictionState[0], stateDict)
                        print "it's safe at", predictionState[0]

                    elif resolution(clauses, notSafe):
                        update(clauses, notSafe, predictionState[0], stateDict)
                        print "NOT safe at", predictionState[0]

        currentState = whereToGo(successors, stateDict, visitedStates)
        visitedStates.append(currentState)
    return problem.reconstructPath(visitedStates)


def whereToGo(successors, stateDict, visitedStates):
    safeStates = set()
    unknownStates = set()

    for successor in successors:
        teleporter = Clause(Literal('t', successor, False))
        safe = Clause(Literal('o', successor, False))
        notSafe = Clause(Literal('o', successor, True))
        if (successor not in visitedStates):
            if teleporter in stateDict.get(successor):
                return successor

            if safe in stateDict.get(successor):
                safeStates.add(successor)

            if (safe and notSafe) not in stateDict.get(successor):
                unknownStates.add(successor)

    if safeStates:
        minState = (100000, 1000000)
        for state in safeStates:
            if stateWeight(state) < stateWeight(minState):
                minState = state
        return minState

    if unknownStates:
        minState = (100000, 1000000)
        for state in unknownStates:
            if stateWeight(state) < stateWeight(minState):
                minState = state
        return minState

    return None


def updateDict(clauses, state, dict):
    oldClauses = dict.get(state)
    if not oldClauses:
        oldClauses = set()
    newClauses = oldClauses.union(clauses)
    dict.update({state: newClauses})


def update(clauses, clause, state, dict):
    oldClauses = dict.get(state)
    if (not oldClauses):
        oldClauses = set()
    oldClauses.add(clause)
    clauses.add(clause)
    dict.update({state: oldClauses})


def scgClause(problem, state):
    stench = problem.isWumpusClose(state)
    chemicals = problem.isPoisonCapsuleClose(state)
    glow = problem.isTeleporterClose(state)
    clauses = set()
    clauses.add(Clause(Literal('b', state, not chemicals)))
    clauses.add(Clause(Literal('g', state, not glow)))
    clauses.add(Clause(Literal('s', state, not stench)))
    return clauses


def ruleClauses(problem, state):
    clauses = set()

    clauses.add(firstRuleClause(problem, state))
    clauses = clauses.union(secondRuleClauses(problem, state))
    clauses.add(thirdRuleClause(problem, state))
    clauses = clauses.union(fourthRuleClauses(problem, state))
    clauses.add(fifthRuleClause(problem, state))
    clauses = clauses.union(sixthRuleClauses(problem, state))
    clauses = clauses.union(eighthRuleClause(problem, state))
    clauses.add(ninthRuleClause(problem, state))
    return clauses


def firstRuleClause(problem, state):
    literals = set()
    literals.add(Literal('s', state, True))
    for successor in problem.getSuccessors(state):
        literals.add(Literal('w', successor[0], False))
    return Clause(literals)


def secondRuleClauses(problem, state):
    clauses = set()
    stench = Literal('s', state, False)

    for successor in problem.getSuccessors(state):
        clauses.add(Clause(set([stench, Literal('w', successor[0], True)])))

    return clauses


def thirdRuleClause(problem, state):
    literals = set()
    literals.add(Literal('b', state, True))
    for successor in problem.getSuccessors(state):
        literals.add(Literal('p', successor[0], False))
    return Clause(literals)


def fourthRuleClauses(problem, state):
    clauses = set()
    fumes = Literal('b', state, False)

    for successor in problem.getSuccessors(state):
        clauses.add(Clause(set([fumes, Literal('p', successor[0], True)])))

    return clauses


def fifthRuleClause(problem, state):
    literals = set()
    literals.add(Literal('g', state, True))
    for successor in problem.getSuccessors(state):
        literals.add(Literal('t', successor[0], False))

    return Clause(literals)


def sixthRuleClauses(problem, state):
    clauses = set()
    glow = Literal('g', state, False)

    for successor in problem.getSuccessors(state):
        clauses.add(Clause(set([glow, Literal('t', successor[0], True)])))

    return clauses


def seventhRuleClause(problem, state):
    # lol
    pass


def eighthRuleClause(problem, state):
    clauses = set()
    noStench = Literal('s', state, False)
    noFumes = Literal('b', state, False)

    for successor in problem.getSuccessors(state):
        clauses.add(Clause(set([noFumes, noStench, Literal('o', successor[0], False)])))

    return clauses


def ninthRuleClause(problem, state):
    return Clause(set([Literal('w', state, False), Literal('p', state, False), Literal('o', state, False)]))


# Abbreviations
lbs = logicBasedSearch
